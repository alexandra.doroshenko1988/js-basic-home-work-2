// 1. Які існують типи даних у Javascript?
// string - рядок. Рядок може бути пустим, чі містити один або більше символів. 
// number - для числових значень (як цілих так і десяткових) меньших за 2 у 53 ступені та більших за -2 у 53 ступені.
// bigInt - для великіх числових значень, більшіх за 2 у 53 ступені та меньших за -2 у 53 ступені. 
// boolean - булевий тип, використовується для зберігання логічних значень - так/true або ні/false.
// null - тип для невідомих значень.
// undefined - означає що значення неприсвоєно.
// object - для складних структур данних.
// symbol - для універсальних ідентифікаторів.

// 2. У чому різниця між == і ===?
// Звичайний оператор порівняння (==) порівнює з приведенням типів, операнди різних типів перетворюються на число.
// А оператор суворої рівності (===) порівнює за значенням без перетворення типів.

// 3.Що таке оператор?
// Це елемент який визначає, яку дію необхідно виконати в операції.



let userName
let userAge

do {
    userName = prompt('What is your name?', userName ? userName : '');
} while (!userName);

do {
    userAge = prompt('What is your age?', userAge ? userAge : '');
} while (!userAge || isNaN(userAge));


if (userAge < 18) {
    alert('You are not allowed to visit this website')
} else if (userAge <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert(`Welcome ${userName}`)
    } else {
        alert('You are not allowed to visit this website')
    }
} else {
    alert(`Welcome ${userName}`)}

